#![no_std]

#[macro_use]
extern crate alloc;

use alloc::string::String;

pub trait Digest<const DIGEST_BYTE_LENGTH: usize> {
    fn update(&mut self, message: &[u8]);
    fn finalize(&mut self) -> [u8; DIGEST_BYTE_LENGTH];

    fn hash_to_bytes(&mut self, message: &[u8]) -> [u8; DIGEST_BYTE_LENGTH] {
        self.update(message);
        self.finalize()
    }
    fn hash_to_lowerhex(&mut self, message: &[u8]) -> String {
        self.hash_to_bytes(message)
            .iter()
            .map(|byte| format!("{:02x}", byte))
            .collect()
    }
    fn hash_to_upperhex(&mut self, message: &[u8]) -> String {
        self.hash_to_bytes(message)
            .iter()
            .map(|byte| format!("{:02X}", byte))
            .collect()
    }
}

#[cfg(test)]
mod tests {
    use alloc::vec::Vec;

    use super::Digest;

    struct Tester(Vec<u8>);
    impl Tester {
        const fn new(bytes: Vec<u8>) -> Tester {
            Self(bytes)
        }
    }
    impl Digest<8> for Tester {
        fn update(&mut self, _: &[u8]) {}
        fn finalize(&mut self) -> [u8; 8] {
            [
                self.0[0], self.0[1], self.0[2], self.0[3], self.0[4], self.0[5], self.0[6],
                self.0[7],
            ]
        }
    }

    #[test]
    fn lower_hex() {
        assert_eq!(
            Tester::new(vec![0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef]).hash_to_lowerhex(&[]),
            "0123456789abcdef"
        );
    }
    #[test]
    fn upper_hex() {
        assert_eq!(
            Tester::new(vec![0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef]).hash_to_upperhex(&[]),
            "0123456789ABCDEF"
        );
    }
}
